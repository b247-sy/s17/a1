/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-show an alert to thank the user for their input.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

function printUserInfo(){
	let fullName = prompt("What is your full name?");
	let age = prompt("How old are you?");
	let address = prompt("Where do you live?");
	alert("Thank you for your entering your details!");
	console.log("Hello, " + name);
	console.log("You are " + age + " years old.");
	console.log("You live in " + address);
}

printUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function printMyTopFiveMusicians(){
	console.log("1. Lady Gaga");
	console.log("2. Gigi De Lana");
	console.log("3. Dua Lipa");
	console.log("4. Mariah Carey");
	console.log("5. Whitney Houston");
}

printMyTopFiveMusicians();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function printFaveMovies() {
	console.log("1. Avatar");
	console.log("Tomatometer score: 82%)");
	console.log("2. Avatar: The way of water");
	console.log("Tomatometer score: 77%");
	console.log("3. Top Gun Maverick");
	console.log("Tomatometer score: 96%");
	console.log("4. Wonder Woman");
	console.log("Tomatometer score: 93%");
	console.log("5. Avatar");
	console.log("Tomatometer score: 90%");
}

printFaveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printFriends();

